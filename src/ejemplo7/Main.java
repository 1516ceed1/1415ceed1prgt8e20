/*
 * Main.java
 *
 * Created on 11 de agosto de 2008, 16:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo7;

/**
 *
 * @author alberto
 */
import java.sql.*;
    
    /**
     * @param args the command line arguments
     */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
        public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            
            //*registrar_el_driver;
            Class.forName(driver).newInstance();
            
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");

            //*Abrir_la_conexion;
             System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"paco","");
            
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            //Uso del m�todo executeQuery
            
            //*crear_sentencia_sin_vision_de_cambios_y_con_modificaciones;
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE); //Todo tipo de movimientos con cambios y s�lo lectura
             
            
            
            //*ejecutar_la_sentencia;
            String sql = "select  * from articulos";
            rs = stmt.executeQuery(sql);
            
            
            
            rs.last();  //Nos desplazamos al �ltimo registro
            System.out.println("Numero de registros en RS: "+rs.getRow()+ " filas");
            System.out.println("Timeout de consulta: "+stmt.getQueryTimeout()+ " sg");
            System.out.println("N�mero m�ximo de registros: "+stmt.getMaxRows());
            System.out.println("Tama�o m�ximo de campo: "+stmt.getMaxFieldSize()+" bytes");
            System.out.println("N�mero de registros devueltos cada vez: "+stmt.getFetchSize());
            
            
        } catch(SQLException se) {
            //Errores de JDBC
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(rs!=null)
                    rs.close();

                if(stmt!=null)
                    stmt.close();

                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
            }//end finally try
        }//end try  
        }
}

    
    
