/*
 * Main.java
 *
 * Created on 9 de septiembre de 2006, 17:58
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo32;

import com.sun.rowset.WebRowSetImpl;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import javax.sql.rowset.WebRowSet;

/**
 *
 * @author alberto
 */
public class Main {
    //Constante que representa el fichero XML 
    private static String WRS_FILE_LOC ="wrs.xml";    

    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        try {
            //Instanciamos el objeto WebRowSet 
            WebRowSet wrs = new WebRowSetImpl();         
            
            //Leemos el fichero XML
            wrs = readXmlFile();
            //Mostramos los valores
            System.out.println("Contenido del fichero actual.");
            while (wrs.next()) {
                System.out.print("ID = "+wrs.getInt(1));
                System.out.print(", Nombre = "+wrs.getString(2));
                System.out.print(", Direccion = "+wrs.getString(3));
                System.out.println();

            }
            wrs.close();
            
            // Actualizamos
            wrs = readXmlFile();
            wrs.moveToInsertRow();
            //A�ade los datos en la nueva fila
            wrs.updateInt(1,5);
            wrs.updateString(2,"Jose");
            wrs.updateString(3,"C/Zamora,3");
            
            //Escribe las filas en el RowSet
            wrs.insertRow();
            wrs.moveToCurrentRow();
            
            //Env�a los cambios a la fuente de datos
            //*wrs.acceptChanges();
            
            //Escribimos el fichero XML
            System.out.print("Añadido registro\n");
            writeXmlFile(wrs);
            
            //Leemos el fichero XML y obtenemos el WebRowSet
            wrs = readXmlFile();
            
            //Mostramos los valores
            System.out.println("Contenido del fichero modificado.");
            while (wrs.next()) {
                System.out.print("ID = "+wrs.getInt(1));
                System.out.print(", Nombre = "+wrs.getString(2));
                System.out.print(", Direccion = "+wrs.getString(3));
                System.out.println();

            }
            wrs.close();
        }catch (SQLException se){
            se.printStackTrace();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void writeXmlFile(WebRowSet wrs) throws SQLException, IOException {
            
            //Escribimos el fichero XML 
            System.out.println("Escribiendo fichero XML: " + WRS_FILE_LOC);
            FileWriter fw = new FileWriter(WRS_FILE_LOC);
            wrs.writeXml(fw);
            fw.close();
            wrs.close();
            
    }

    public static WebRowSet readXmlFile() throws SQLException, IOException {
        WebRowSet wrs2 = new WebRowSetImpl();
        //Leemos el fichero
        FileReader fr = new FileReader(WRS_FILE_LOC);
        //Rellenamos el WebRowSet a partir del fichero XML
        wrs2.readXml(fr);        
        return wrs2;
    }

}
