/*
 * Main.java
 *
 * Created on 12 de agosto de 2008, 11:45
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo10;

import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;
    Statement stmt = null;

    try {
      //Registrando el Driver
      String driver = "com.mysql.jdbc.Driver";
      Class.forName(driver).newInstance();
      System.out.println("Driver " + driver + " Registrado correctamente");

      //Abrir la conexi�n con la Base de Datos
      System.out.println("Conectando con la Base de datos...");
      String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
      conn = DriverManager.getConnection(jdbcUrl, "paco", "");
      System.out.println("Conexi�n establecida con la Base de datos...");

      conn.setAutoCommit(false);
      stmt = conn.createStatement();

      //A�adimos sentencias SQL a modo Batch
      String sql = "insert into articulos values(8,'HD 120G',100.0,'HD120',2)";
      stmt.addBatch(sql);
      sql = "insert into articulos values(9,'HD 160G',120.0,'HD160',2)";
      stmt.addBatch(sql);
      sql = "insert into articulos values(10,'HD 200G',140.0,'HD200',2)";
      stmt.addBatch(sql);
      sql = "update articulos set precio=precio*1.05 where id=8";
      stmt.addBatch(sql);
      sql = "update articulos set precio=precio*1.06 where id=9";
      stmt.addBatch(sql);
      sql = "update articulos set precio=precio*1.07 where id=10";
      stmt.addBatch(sql);

      int result[] = stmt.executeBatch();
      conn.commit();

      for (int i = 0; i < result.length; i++) {
        System.out.println("Sentencia [" + i + "]: resultado: " + result[i] + " OK");
      }



    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        conn.rollback();

        if (stmt != null) {
          stmt.close();
        }

        if (conn != null) {
          conn.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try  
  }
}
